﻿namespace PackViagens
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;

    public partial class Remover : Form
    {
        private List<Packs> listaCopiada = new List<Packs>();

        public Remover(List<Packs> ListaDeViagens)
        {
            InitializeComponent();
            listaCopiada = ListaDeViagens;
            foreach (var pack in listaCopiada)
            {
                cbRemover.Items.Add(pack.Descricao);
            }
        }

        private void btConfirmar_Click(object sender, EventArgs e)
        {
            if (cbRemover.SelectedIndex == -1)
            {
                MessageBox.Show("Tem que escolher um pack de viagens");
                return;
            }
            listaCopiada.RemoveAt(cbRemover.SelectedIndex);
            MessageBox.Show("Pack de viagem apagado com sucesso");
            Close();
        }
       
        private void btFechar4_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
