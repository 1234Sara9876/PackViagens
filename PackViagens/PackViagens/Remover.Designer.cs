﻿namespace PackViagens
{
    partial class Remover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbRemover = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btFechar4 = new System.Windows.Forms.Button();
            this.btConfirmar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbRemover
            // 
            this.cbRemover.FormattingEnabled = true;
            this.cbRemover.Location = new System.Drawing.Point(31, 101);
            this.cbRemover.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemover.Name = "cbRemover";
            this.cbRemover.Size = new System.Drawing.Size(244, 21);
            this.cbRemover.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(67, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Seleccionar o pack:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(167, 193);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(8, 6);
            this.button2.TabIndex = 15;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btFechar4
            // 
            this.btFechar4.Image = global::PackViagens.Properties.Resources.Close2;
            this.btFechar4.Location = new System.Drawing.Point(270, 193);
            this.btFechar4.Margin = new System.Windows.Forms.Padding(2);
            this.btFechar4.Name = "btFechar4";
            this.btFechar4.Size = new System.Drawing.Size(33, 36);
            this.btFechar4.TabIndex = 16;
            this.btFechar4.UseVisualStyleBackColor = true;
            this.btFechar4.Click += new System.EventHandler(this.btFechar4_Click);
            // 
            // btConfirmar
            // 
            this.btConfirmar.Image = global::PackViagens.Properties.Resources.Save;
            this.btConfirmar.Location = new System.Drawing.Point(221, 193);
            this.btConfirmar.Margin = new System.Windows.Forms.Padding(2);
            this.btConfirmar.Name = "btConfirmar";
            this.btConfirmar.Size = new System.Drawing.Size(33, 36);
            this.btConfirmar.TabIndex = 14;
            this.btConfirmar.UseVisualStyleBackColor = true;
            this.btConfirmar.Click += new System.EventHandler(this.btConfirmar_Click);
            // 
            // Remover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 228);
            this.ControlBox = false;
            this.Controls.Add(this.btFechar4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btConfirmar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbRemover);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Remover";
            this.Text = "Remover";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbRemover;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btFechar4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btConfirmar;
    }
}