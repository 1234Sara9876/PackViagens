﻿namespace PackViagens
{
    partial class FormViagens
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFicheiro = new System.Windows.Forms.ToolStripMenuItem();
            this.ddSaveFile = new System.Windows.Forms.ToolStripMenuItem();
            this.ddOpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.btFechar = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btRead = new System.Windows.Forms.Button();
            this.btCreate = new System.Windows.Forms.Button();
            this.tbddNome = new System.Windows.Forms.ToolStripTextBox();
            this.tbddLocal = new System.Windows.Forms.ToolStripTextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAbout,
            this.tsmFicheiro});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(516, 24);
            this.menuStrip1.TabIndex = 4;
            // 
            // tsmAbout
            // 
            this.tsmAbout.Name = "tsmAbout";
            this.tsmAbout.Size = new System.Drawing.Size(52, 20);
            this.tsmAbout.Text = "About";
            this.tsmAbout.Click += new System.EventHandler(this.tsmAbout_Click);
            // 
            // tsmFicheiro
            // 
            this.tsmFicheiro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddSaveFile,
            this.ddOpenFile});
            this.tsmFicheiro.Name = "tsmFicheiro";
            this.tsmFicheiro.Size = new System.Drawing.Size(61, 20);
            this.tsmFicheiro.Text = "Ficheiro";
            // 
            // ddSaveFile
            // 
            this.ddSaveFile.Name = "ddSaveFile";
            this.ddSaveFile.Size = new System.Drawing.Size(152, 22);
            this.ddSaveFile.Text = "Gravar";
            this.ddSaveFile.Click += new System.EventHandler(this.ddSaveFile_Click);
            // 
            // ddOpenFile
            // 
            this.ddOpenFile.Name = "ddOpenFile";
            this.ddOpenFile.Size = new System.Drawing.Size(152, 22);
            this.ddOpenFile.Text = "Abrir";
            this.ddOpenFile.Click += new System.EventHandler(this.ddOpenFile_Click);
            // 
            // btFechar
            // 
            this.btFechar.Image = global::PackViagens.Properties.Resources.Close2;
            this.btFechar.Location = new System.Drawing.Point(465, 328);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(50, 46);
            this.btFechar.TabIndex = 5;
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // btDelete
            // 
            this.btDelete.Image = global::PackViagens.Properties.Resources.Remove;
            this.btDelete.Location = new System.Drawing.Point(292, 195);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(75, 61);
            this.btDelete.TabIndex = 3;
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btUpdate
            // 
            this.btUpdate.Image = global::PackViagens.Properties.Resources.A;
            this.btUpdate.Location = new System.Drawing.Point(159, 195);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(75, 61);
            this.btUpdate.TabIndex = 2;
            this.btUpdate.UseVisualStyleBackColor = true;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btRead
            // 
            this.btRead.Image = global::PackViagens.Properties.Resources.V;
            this.btRead.Location = new System.Drawing.Point(292, 95);
            this.btRead.Name = "btRead";
            this.btRead.Size = new System.Drawing.Size(75, 64);
            this.btRead.TabIndex = 1;
            this.btRead.UseVisualStyleBackColor = true;
            this.btRead.Click += new System.EventHandler(this.btRead_Click);
            // 
            // btCreate
            // 
            this.btCreate.Image = global::PackViagens.Properties.Resources.Create;
            this.btCreate.Location = new System.Drawing.Point(159, 95);
            this.btCreate.Name = "btCreate";
            this.btCreate.Size = new System.Drawing.Size(75, 64);
            this.btCreate.TabIndex = 0;
            this.btCreate.UseVisualStyleBackColor = true;
            this.btCreate.Click += new System.EventHandler(this.btCreate_Click);
            // 
            // tbddNome
            // 
            this.tbddNome.Name = "tbddNome";
            this.tbddNome.Size = new System.Drawing.Size(100, 23);
            // 
            // tbddLocal
            // 
            this.tbddLocal.Name = "tbddLocal";
            this.tbddLocal.Size = new System.Drawing.Size(100, 23);
            // 
            // FormViagens
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 375);
            this.ControlBox = false;
            this.Controls.Add(this.btFechar);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btUpdate);
            this.Controls.Add(this.btRead);
            this.Controls.Add(this.btCreate);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormViagens";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agência de Viagens";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btCreate;
        private System.Windows.Forms.Button btRead;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAbout;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.ToolStripMenuItem tsmFicheiro;
        private System.Windows.Forms.ToolStripMenuItem ddSaveFile;
        private System.Windows.Forms.ToolStripMenuItem ddOpenFile;
        private System.Windows.Forms.ToolStripTextBox tbddNome;
        private System.Windows.Forms.ToolStripTextBox tbddLocal;
    }
}

