﻿namespace PackViagens
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;

    public partial class Ver : Form
    {
        private List<Packs> listaCopiada = new List<Packs>();


        public Ver(List<Packs> ListaDeViagens)
        {
            InitializeComponent();
            listaCopiada = ListaDeViagens;
            for (int i = 0; i < listaCopiada.Count ;i++)
            {
              dgview.Rows.Add( listaCopiada[i].IdPack,
                             listaCopiada[i].Descricao,
                             listaCopiada[i].Preco );
            }
           
        }

        private void btFechar2_Click(object sender, EventArgs e)
        {
            Close();
        }

       
    }
}
