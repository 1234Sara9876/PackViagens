﻿namespace PackViagens
{
    partial class Ver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgview = new System.Windows.Forms.DataGridView();
            this.ID_Pack = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descrição_Viagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Preço_Viagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btFechar2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgview)).BeginInit();
            this.SuspendLayout();
            // 
            // dgview
            // 
            this.dgview.AllowUserToAddRows = false;
            this.dgview.AllowUserToDeleteRows = false;
            this.dgview.AllowUserToOrderColumns = true;
            this.dgview.AllowUserToResizeColumns = false;
            this.dgview.AllowUserToResizeRows = false;
            this.dgview.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Pack,
            this.Descrição_Viagem,
            this.Preço_Viagem});
            this.dgview.Location = new System.Drawing.Point(15, 14);
            this.dgview.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgview.Name = "dgview";
            this.dgview.ReadOnly = true;
            this.dgview.RowTemplate.Height = 24;
            this.dgview.Size = new System.Drawing.Size(708, 420);
            this.dgview.TabIndex = 0;
            // 
            // ID_Pack
            // 
            this.ID_Pack.HeaderText = "ID_Pack";
            this.ID_Pack.Name = "ID_Pack";
            this.ID_Pack.ReadOnly = true;
            // 
            // Descrição_Viagem
            // 
            this.Descrição_Viagem.HeaderText = "Descrição_Viagem";
            this.Descrição_Viagem.Name = "Descrição_Viagem";
            this.Descrição_Viagem.ReadOnly = true;
            // 
            // Preço_Viagem
            // 
            this.Preço_Viagem.HeaderText = "Preço_Viagem";
            this.Preço_Viagem.Name = "Preço_Viagem";
            this.Preço_Viagem.ReadOnly = true;
            // 
            // btFechar2
            // 
            this.btFechar2.Image = global::PackViagens.Properties.Resources.Close2;
            this.btFechar2.Location = new System.Drawing.Point(688, 464);
            this.btFechar2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btFechar2.Name = "btFechar2";
            this.btFechar2.Size = new System.Drawing.Size(49, 42);
            this.btFechar2.TabIndex = 1;
            this.btFechar2.UseVisualStyleBackColor = true;
            this.btFechar2.Click += new System.EventHandler(this.btFechar2_Click);
            // 
            // Ver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 505);
            this.ControlBox = false;
            this.Controls.Add(this.btFechar2);
            this.Controls.Add(this.dgview);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Ver";
            this.Text = "Ver";
            ((System.ComponentModel.ISupportInitialize)(this.dgview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgview;
        private System.Windows.Forms.Button btFechar2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Pack;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descrição_Viagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Preço_Viagem;
    }
}