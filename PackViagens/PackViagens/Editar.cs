﻿namespace PackViagens
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class Editar : Form
    {
        private List<Packs> listaCopiada = new List<Packs>();
        int index;

        public Editar(List<Packs> ListaDeViagens)
        {
            InitializeComponent();
            listaCopiada = ListaDeViagens;
            index = 0;
            if (listaCopiada.Count > 0)
            {
                MostraLista();
            }
            else
            {
                MessageBox.Show("A lista não tem packs de viagens; tem de sair e criar um pack");
            }
        }

        private void btForward_Click(object sender, EventArgs e)
        {
            if (index < listaCopiada.Count-1)
            {
                index++;
                MostraLista();
            }
            else
            {
                MessageBox.Show("A lista não tem mais itens");
            }
        }

        private void btBack_Click(object sender, EventArgs e)
        {
            if (index > 0)
            {
                index--;
                MostraLista();
            }
            else
            {
                MessageBox.Show("A lista não tem mais itens");
            }
        }

        private void MostraLista()
        {
            tbIDPack2.Text =listaCopiada[index].IdPack.ToString();
            rtbDesc2.Text = listaCopiada[index].Descricao;
            udPreco2.Value = listaCopiada[index].Preco;
        }

        private void btGravar_Click(object sender, EventArgs e)
        {
           for (int i=0; i <= listaCopiada.Count-1; i++)
            {
                if (listaCopiada[i].IdPack == Convert.ToInt16(tbIDPack2.Text))
                {
                    listaCopiada[i].Descricao = rtbDesc2.Text;
                    listaCopiada[i].Preco = udPreco2.Value;
                    MessageBox.Show("Pack de viagem alterado com sucesso");
                    Close();
                }
            }
        }
       
    }
}
