﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackViagens.Modelos
{
    public class Packs
    {
        #region Atributos e Propriedades condensados

        public int IdPack { get; set; }

        public string Descricao { get; set; }

        public decimal Preco { get; set; }

        #endregion

        #region outros métodos
        

        public override string ToString()
        {
            return string.Format("ID Pack:{0} Nome:{1} Preço:{2}", IdPack, Descricao, Preco);
        }
        #endregion
    }
}
