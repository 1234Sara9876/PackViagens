﻿

namespace PackViagens
{
    using System;
    using System.Windows.Forms;
    using System.Collections.Generic;
    using Modelos;
    using System.IO;

    public partial class FormViagens : Form
    {
        #region Variáveis(lista e formulários)

        private List<Packs> ListaDeViagens = new List<Packs>();
        private Criar fCriar;
        private Ver fVer;
        private Editar fEditar;
        private Remover fRemover;
        //string filename;
        //string local;

        #endregion

        #region construtor do form

        public FormViagens()
        {
            InitializeComponent();
            if (!CarregarViagens())
            {
                ListaDeViagens.Add(new Packs { IdPack = 1, Descricao = "Sete dias na Patagónia", Preco = 402.50m });
                ListaDeViagens.Add(new Packs { IdPack = 2, Descricao = "Catorze dias na China", Preco = 3500.00m });
            }
            //os botões e menustrip (tsmAbout) estão default enabled true
        }
        #endregion

        #region Métodos dos componentes

        private void tsmAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Projeto PackViagens - Sara Cardoso em 8/10/2017, V 1.0.0");
        }

        private void btCreate_Click(object sender, EventArgs e)
        {
            fCriar = new Criar(ListaDeViagens);
            fCriar.Show();
        }

        private void btRead_Click(object sender, EventArgs e)
        {
            fVer = new Ver(ListaDeViagens);
            fVer.Show();
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            fEditar = new Editar(ListaDeViagens);
            fEditar.Show();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            fRemover = new Remover(ListaDeViagens);
            fRemover.Show();
        }
       
        private void ddSaveFile_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Em seguida poderá optar por criar um novo ficheiro ou reescrever um já existente");
            if (GravarViagens())
            {
                MessageBox.Show("Gravação bem sucedida");
            }
        }

        private void ddOpenFile_Click(object sender, EventArgs e)
        {
            if (CarregarViagens())
            {
                MessageBox.Show("Ficheiro aberto à escrita");
            }
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        private bool CarregarViagens()
        {
            StreamReader myStream;
            OpenFileDialog ficheiro = new OpenFileDialog();
          
            ficheiro.InitialDirectory = "c:\\";
            ficheiro.Filter = "Text file|*.txt";
            ficheiro.FilterIndex = 2;
            ficheiro.RestoreDirectory = true;

            if (ficheiro.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = new StreamReader(ficheiro.OpenFile())) != null)
                    {
                        using (myStream)
                        {
                            ListaDeViagens.Clear();
                            string linha = "";
                            while ((linha = myStream.ReadLine()) != null)
                            {
                                string[] campos = new string[3];
                                campos = linha.Split(';');
                                var pack = new Packs
                                {
                                    IdPack = Convert.ToInt32(campos[0]),
                                    Descricao = campos[1],
                                    Preco = Convert.ToDecimal(campos[2])
                                };
                                ListaDeViagens.Add(pack);
                            }
                            myStream.Close();
                        }
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Não se encontrou o ficheiro no disco" + ex.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        private bool GravarViagens()
        {
            SaveFileDialog ficheiro = new SaveFileDialog();
            ficheiro.Filter = "Text file|*.txt";
            ficheiro.Title = "Gravar ficheiro de texto";
            ficheiro.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (ficheiro.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                FileStream fs = (FileStream)ficheiro.OpenFile();
                StreamWriter sw = new StreamWriter(fs);

                //verficar se existe algum problema
                foreach (var pack in ListaDeViagens)
                {
                    string linha = string.Format("{0};{1};{2}", pack.IdPack, pack.Descricao, pack.Preco);
                    sw.WriteLine(linha);
                }
                sw.Close();
                fs.Close();

                return true;
            }
            return false;
        }

        //private bool GravarViagens(), da maneira anterior
        //string ficheiro = @"Packs.txt";
        //StreamReader sr;
        //try
        //{
        //    if (File.Exists(ficheiro))
        //    {
        //        sr = File.OpenText(ficheiro);
        //        string linha = "";
        //        while ((linha = sr.ReadLine()) != null)
        //        {
        //            string[] campos = new string[3];
        //            campos = linha.Split(';');
        //            var viagem = new Packs
        //            {
        //                IdPack = Convert.ToInt32(campos[0]),
        //                Descricao = campos[1],
        //                Preco = Convert.ToDecimal(campos[2])
        //            };
        //            ListaDeViagens.Add(viagem);
        //        }
        //        sr.Close();
        //    }
        //    else
        //        return false;
        //}
        //catch (Exception e)
        //{
        //    MessageBox.Show(e.Message);
        //    return false;
        //}
        //return true;

        //private bool CarregarViagens(), da maneira anterior
        //    string ficheiro = string.Empty;
        //    if (filename == string.Empty)
        //    {
        //        if (local == string.Empty)
        //            ficheiro = @"Packs.txt";
        //        else
        //            ficheiro = @"${local}"+@"Packs.txt";
        //    }
        //    else
        //    {
        //        if (local == string.Empty)
        //            ficheiro = @"${filename}.txt";
        //        else
        //            ficheiro = @"${local}"+@"${filename}.txt";
        //    }
        //    //streamwriter tem como argumentos (string path, bool append), se append fôr false cria o ficheiro, se append fôr true faz overwrite;por defeito é false
        //    StreamWriter sw = new StreamWriter(ficheiro, false);
        //    //try-catch lança uma excepção, se o ficheiro n ficar gravado
        //    try
        //    {
        //        if (!File.Exists(ficheiro))
        //            sw = File.CreateText(ficheiro);
        //        foreach (var viagem in ListaDeViagens)
        //        {
        //            string linha = string.Format("{0};{1};{2}", viagem.IdPack,
        //            viagem.Descricao, viagem.Preco);
        //            sw.WriteLine(linha);
        //        }
        //        sw.Close();
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    return true;

        //private void ddtsmNome_Click(object sender, EventArgs e)
        //{
        //    MessageBox.Show("Introduza o nome do ficheiro de packs-viagem");
        //    if (!string.IsNullOrEmpty(tbddNome.Text))
        //    {
        //        filename = tbddNome.Text;
        //    }
        //}

        //private void ddtsmLocal_Click(object sender, EventArgs e)
        //{
        //    MessageBox.Show("Introduza a localização do ficheiro de packs-viagem");
        //    if (!string.IsNullOrEmpty(tbddLocal.Text))
        //    {
        //        local = tbddLocal.Text;
        //    }
        //}
    }
}
