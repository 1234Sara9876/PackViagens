﻿namespace PackViagens
{
    partial class Editar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbIDPack2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rtbDesc2 = new System.Windows.Forms.RichTextBox();
            this.btGravar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btForward = new System.Windows.Forms.Button();
            this.btBack = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.udPreco2 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.udPreco2)).BeginInit();
            this.SuspendLayout();
            // 
            // tbIDPack2
            // 
            this.tbIDPack2.Location = new System.Drawing.Point(166, 24);
            this.tbIDPack2.Margin = new System.Windows.Forms.Padding(2);
            this.tbIDPack2.Name = "tbIDPack2";
            this.tbIDPack2.ReadOnly = true;
            this.tbIDPack2.Size = new System.Drawing.Size(40, 20);
            this.tbIDPack2.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "ID Pack";
            // 
            // rtbDesc2
            // 
            this.rtbDesc2.Location = new System.Drawing.Point(20, 61);
            this.rtbDesc2.Margin = new System.Windows.Forms.Padding(2);
            this.rtbDesc2.Name = "rtbDesc2";
            this.rtbDesc2.Size = new System.Drawing.Size(261, 87);
            this.rtbDesc2.TabIndex = 7;
            this.rtbDesc2.Text = "";
            // 
            // btGravar
            // 
            this.btGravar.Image = global::PackViagens.Properties.Resources.Save;
            this.btGravar.Location = new System.Drawing.Point(285, 261);
            this.btGravar.Margin = new System.Windows.Forms.Padding(2);
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(33, 36);
            this.btGravar.TabIndex = 11;
            this.btGravar.UseVisualStyleBackColor = true;
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(166, 210);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(8, 6);
            this.button2.TabIndex = 12;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btForward
            // 
            this.btForward.Image = global::PackViagens.Properties.Resources.Forward;
            this.btForward.Location = new System.Drawing.Point(148, 251);
            this.btForward.Margin = new System.Windows.Forms.Padding(2);
            this.btForward.Name = "btForward";
            this.btForward.Size = new System.Drawing.Size(26, 36);
            this.btForward.TabIndex = 10;
            this.btForward.UseVisualStyleBackColor = true;
            this.btForward.Click += new System.EventHandler(this.btForward_Click);
            // 
            // btBack
            // 
            this.btBack.Image = global::PackViagens.Properties.Resources.Back;
            this.btBack.Location = new System.Drawing.Point(59, 251);
            this.btBack.Margin = new System.Windows.Forms.Padding(2);
            this.btBack.Name = "btBack";
            this.btBack.Size = new System.Drawing.Size(23, 36);
            this.btBack.TabIndex = 9;
            this.btBack.UseVisualStyleBackColor = true;
            this.btBack.Click += new System.EventHandler(this.btBack_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(260, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 23);
            this.label2.TabIndex = 14;
            this.label2.Text = "€";
            // 
            // udPreco2
            // 
            this.udPreco2.DecimalPlaces = 2;
            this.udPreco2.Location = new System.Drawing.Point(166, 175);
            this.udPreco2.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            131072});
            this.udPreco2.Name = "udPreco2";
            this.udPreco2.Size = new System.Drawing.Size(88, 20);
            this.udPreco2.TabIndex = 15;
            // 
            // Editar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 298);
            this.ControlBox = false;
            this.Controls.Add(this.udPreco2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btGravar);
            this.Controls.Add(this.btForward);
            this.Controls.Add(this.btBack);
            this.Controls.Add(this.rtbDesc2);
            this.Controls.Add(this.tbIDPack2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Editar";
            this.Text = "Editar";
            ((System.ComponentModel.ISupportInitialize)(this.udPreco2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbIDPack2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtbDesc2;
        private System.Windows.Forms.Button btBack;
        private System.Windows.Forms.Button btForward;
        private System.Windows.Forms.Button btGravar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown udPreco2;
    }
}