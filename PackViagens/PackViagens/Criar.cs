﻿namespace PackViagens
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;

    public partial class Criar : Form
    {
        private List<Packs> listaCopiada = new List<Packs>();

        public Criar (List<Packs> ListaDeViagens)
        {
            InitializeComponent();
            listaCopiada = ListaDeViagens;
            tbIDPack.Text = GeraIdPack().ToString();//este seria um método de componente, mas tem de ficar embutido no construtor do formulário para aparecer assim que este é mostrado
        }

        #region Métodos dos componentes
        private void btSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(rtbDesc.Text))
            {
                MessageBox.Show("Tem que inserir a descriçao da viagem");
                return;
            }
            if (udPreco.Value<=0)
            {
                MessageBox.Show("Introduza o preço do novo pack de viagem");
                return;
            }
           

            var pack = new Packs
            {
                IdPack = GeraIdPack(),
                Descricao = rtbDesc.Text,
                Preco = udPreco.Value
            };
            listaCopiada.Add(pack);
            MessageBox.Show("Pack de viagem gravado com sucesso");
            Hide();
        }

        private void btFechar1_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region Outros métodos
        private int GeraIdPack()
        {
            if (listaCopiada.Count > 0)
            {
                return listaCopiada[listaCopiada.Count - 1].IdPack + 1;
            }
            else
            {
                return 1;
            }
        }
       
        #endregion
    }
}
