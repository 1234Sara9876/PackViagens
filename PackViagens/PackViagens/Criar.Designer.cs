﻿namespace PackViagens
{
    partial class Criar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbIDPack = new System.Windows.Forms.TextBox();
            this.rtbDesc = new System.Windows.Forms.RichTextBox();
            this.btSave = new System.Windows.Forms.Button();
            this.btFechar1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.udPreco = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.udPreco)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(107, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Pack";
            // 
            // tbIDPack
            // 
            this.tbIDPack.BackColor = System.Drawing.SystemColors.Window;
            this.tbIDPack.Location = new System.Drawing.Point(194, 23);
            this.tbIDPack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbIDPack.Name = "tbIDPack";
            this.tbIDPack.ReadOnly = true;
            this.tbIDPack.Size = new System.Drawing.Size(40, 20);
            this.tbIDPack.TabIndex = 1;
            // 
            // rtbDesc
            // 
            this.rtbDesc.Location = new System.Drawing.Point(19, 59);
            this.rtbDesc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rtbDesc.Name = "rtbDesc";
            this.rtbDesc.Size = new System.Drawing.Size(302, 116);
            this.rtbDesc.TabIndex = 4;
            this.rtbDesc.Text = "";
            // 
            // btSave
            // 
            this.btSave.Image = global::PackViagens.Properties.Resources.Save;
            this.btSave.Location = new System.Drawing.Point(258, 272);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(40, 33);
            this.btSave.TabIndex = 6;
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btFechar1
            // 
            this.btFechar1.Image = global::PackViagens.Properties.Resources.Close2;
            this.btFechar1.Location = new System.Drawing.Point(312, 272);
            this.btFechar1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btFechar1.Name = "btFechar1";
            this.btFechar1.Size = new System.Drawing.Size(36, 33);
            this.btFechar1.TabIndex = 5;
            this.btFechar1.UseVisualStyleBackColor = true;
            this.btFechar1.Click += new System.EventHandler(this.btFechar1_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(308, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "€";
            // 
            // udPreco
            // 
            this.udPreco.DecimalPlaces = 2;
            this.udPreco.Location = new System.Drawing.Point(217, 203);
            this.udPreco.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            131072});
            this.udPreco.Name = "udPreco";
            this.udPreco.Size = new System.Drawing.Size(85, 20);
            this.udPreco.TabIndex = 9;
            // 
            // Criar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 306);
            this.ControlBox = false;
            this.Controls.Add(this.udPreco);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btFechar1);
            this.Controls.Add(this.rtbDesc);
            this.Controls.Add(this.tbIDPack);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Criar";
            this.Text = "Criar";
            ((System.ComponentModel.ISupportInitialize)(this.udPreco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbIDPack;
        private System.Windows.Forms.RichTextBox rtbDesc;
        private System.Windows.Forms.Button btFechar1;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown udPreco;
    }
}